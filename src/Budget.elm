module Budget
    exposing
        ( Budget
        , Id
        , addNewCategory
        , empty
        )

import Amount exposing (Amount)
import Category exposing (Category)
import Money
import Month exposing (Month)


type Id
    = Id Int


type alias Budget =
    { id : Id
    , name : String
    , balance : Amount
    , categories : List Category
    }


empty : Budget
empty =
    { id = Id -1
    , name = ""
    , balance = Amount.zero Money.CHF
    , categories = []
    }


addNewCategory : Month -> Budget -> Budget
addNewCategory month budget =
    let
        category =
            Category.addNewMonthlyBudget month Category.empty
    in
    { budget | categories = category :: budget.categories }
