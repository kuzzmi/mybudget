module Router exposing (Route(..), link, newUrl, parse)

import Html.Styled exposing (Attribute, Html, a)
import Html.Styled.Attributes exposing (href)
import Html.Styled.Events exposing (onWithOptions)
import Json.Decode as Decode
import Navigation exposing (Location)
import UrlParser exposing ((</>), Parser, map, oneOf, s)


type Route
    = Root
    | NotFound
    | NewBudget


route : Parser (Route -> a) a
route =
    oneOf
        [ map Root UrlParser.top
        , map NewBudget (s "budgets" </> s "new")
        ]


routeToString : Route -> String
routeToString route =
    let
        pieces =
            case route of
                Root ->
                    []

                NotFound ->
                    [ "404" ]

                NewBudget ->
                    [ "budgets", "new" ]
    in
    "/" ++ String.join "/" pieces



-- Public helpers


newUrl : Route -> Cmd msg
newUrl =
    routeToString >> Navigation.newUrl


parse : Location -> Route
parse =
    Maybe.withDefault NotFound << UrlParser.parsePath route


link : (Route -> msg) -> Route -> List (Attribute msg) -> List (Html msg) -> Html msg
link msg route attributes =
    let
        options =
            { stopPropagation = False
            , preventDefault = True
            }

        onLinkClick =
            onWithOptions
                "click"
                options
                (preventDefaultUnlessKeyPressed
                    |> Decode.andThen (maybePreventDefault (msg route))
                )

        preventDefaultUnlessKeyPressed =
            Decode.map2
                nor
                (Decode.field "ctrlKey" Decode.bool)
                (Decode.field "metaKey" Decode.bool)

        nor x y =
            not (x || y)

        maybePreventDefault msg preventDefault =
            if preventDefault then
                Decode.succeed msg
            else
                Decode.fail "Delegated to browser default"

        attrs =
            [ onLinkClick, href <| routeToString route ] ++ attributes
    in
    a attrs
