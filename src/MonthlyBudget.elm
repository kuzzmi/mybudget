module MonthlyBudget exposing (Id, MonthlyBudget, balance, empty)

import Amount exposing (Amount)
import Money
import Transaction exposing (Transaction(..))


type Id
    = Id Int


type alias MonthlyBudget =
    { id : Id
    , transactions : List Transaction
    , budgeted : Amount
    }


empty : MonthlyBudget
empty =
    { id = Id -1
    , transactions = []
    , budgeted = Amount.zero Money.CHF
    }


balance : MonthlyBudget -> Amount
balance { transactions, budgeted } =
    let
        getVal t =
            case t of
                Income _ amount ->
                    amount

                Expenditure _ amount ->
                    Amount.negate amount

        vals =
            List.map getVal transactions
    in
    List.foldl Amount.add budgeted vals
