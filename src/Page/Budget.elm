module Page.Budget exposing (Model, Msg, init, update, view)

-- import Budget exposing (Budget)

import Field exposing (Field)
import Html.Styled as Html exposing (..)
import Html.Styled.Attributes exposing (type_, value)
import Html.Styled.Events exposing (onBlur, onFocus, onInput)


type alias Form =
    { name : Field String
    , budget : Field Int
    }


form : Form
form =
    { name = Field.empty
    , budget = Field.empty
    }


type alias Model =
    { form : Form
    }


init : Model
init =
    { form = form
    }


type FormMsg
    = UpdateName String
    | UpdateBudget String


type Msg
    = UpdateForm FormMsg


updateForm : FormMsg -> Form -> Form
updateForm msg form =
    case msg of
        UpdateName name ->
            let
                validate a =
                    if String.length a < 10 then
                        Nothing
                    else
                        Just "Too short or too long"

                newName =
                    Field.set name form.name validate
            in
            { form | name = newName }

        UpdateBudget str ->
            let
                validate _ =
                    Nothing

                budget =
                    Result.withDefault 0 <| String.toInt str
            in
            { form | budget = Field.set budget form.budget validate }


update : Msg -> Model -> ( Model, Cmd msg )
update msg model =
    case msg of
        UpdateForm formMsg ->
            let
                form =
                    updateForm formMsg model.form
            in
            ( { model | form = form }, Cmd.none )


viewForm : Form -> Html FormMsg
viewForm form =
    div []
        [ input
            [ onInput UpdateName
            , value <| Field.withDefault "" form.name
            ]
            []
        , input
            [ onInput UpdateBudget
            , type_ "number"
            , value <| toString <| Field.withDefault 0 form.budget
            ]
            []
        ]


view : Model -> Html Msg
view model =
    div []
        [ h2 [] [ text "Form" ]
        , Html.map UpdateForm <| viewForm model.form
        ]
