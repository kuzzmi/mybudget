module Page.Root exposing (Msg, update, view)

import Debug
import Html.Styled exposing (..)
import Router exposing (Route(..), link, newUrl)


type Msg
    = Navigate Route


update : Msg -> Cmd msg
update msg =
    case msg of
        Navigate route ->
            newUrl route


view : Html Msg
view =
    div []
        [ h2 [] [ text "This is Root" ]
        , link Navigate NewBudget [] [ text "Add new budget" ]
        ]
