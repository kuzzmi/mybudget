module Page.MonthlyView
    exposing
        ( Model
        , initCmd
        , initModel
        , update
        , view
        )

import Amount exposing (Amount)
import Budget exposing (Budget)
import Category exposing (Category)
import Css exposing (..)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (css)
import Html.Styled.Events exposing (onClick)
import Month exposing (Month)
import MonthlyBudget exposing (MonthlyBudget)
import Transaction exposing (Transaction(..))


type Msg
    = AddCategory
    | PrevMonth
    | NextMonth
    | GenerateTransaction Category
    | GetRandomTransaction Category Transaction


type alias Model =
    { month : Month
    , budget : Budget
    }


initModel : Month -> Budget -> Model
initModel =
    Model


initCmd : Cmd msg
initCmd =
    Cmd.none


update : Msg -> Model -> ( Model, Cmd Msg )
update action model =
    case action of
        PrevMonth ->
            ( { model | month = Month.prev model.month }, Cmd.none )

        NextMonth ->
            ( { model | month = Month.next model.month }, Cmd.none )

        GenerateTransaction category ->
            ( model, Transaction.generate <| GetRandomTransaction category )

        GetRandomTransaction category t ->
            let
                categoryWithTransaction =
                    Category.addTransaction t model.month category

                { budget } =
                    model

                updateCategory c =
                    if category == c then
                        categoryWithTransaction
                    else
                        c

                updatedCategories =
                    List.map updateCategory model.budget.categories

                updatedBudget =
                    { budget | categories = updatedCategories }
            in
            ( { model | budget = updatedBudget }, Cmd.none )

        AddCategory ->
            ( { model
                | budget = Budget.addNewCategory model.month model.budget
              }
            , Cmd.none
            )


viewTransaction : Transaction -> Html msg
viewTransaction t =
    let
        style =
            case t of
                Income _ _ ->
                    css [ color (rgb 0 120 0) ]

                Expenditure _ _ ->
                    css [ color (rgb 120 0 0) ]
    in
    div [ style ]
        [ text <| Amount.toString <| Transaction.value t
        ]


viewMonthlyBudget : Maybe MonthlyBudget -> Html msg
viewMonthlyBudget monthlyBudget =
    let
        transactions =
            case monthlyBudget of
                Just mb ->
                    if List.length mb.transactions == 0 then
                        div [] [ text "No transactions" ]
                    else
                        div [] <| List.map viewTransaction mb.transactions

                Nothing ->
                    div [] [ text "No budget yet for this category" ]

        balance =
            case monthlyBudget of
                Just mb ->
                    Amount.toString <| MonthlyBudget.balance mb

                Nothing ->
                    "0"
    in
    div []
        [ h5 [] [ text balance ]

        --, transactions
        ]



--
-- , transactions


viewCategory : Month -> Category -> Html Msg
viewCategory month category =
    div []
        [ h3 [] [ text category.name ]
        , button [ onClick <| GenerateTransaction category ] [ text "generate transaction" ]
        , viewMonthlyBudget <| Category.getMonthlyBudget month category
        ]


viewBudget : Budget -> Html Msg
viewBudget budget =
    div []
        [ h1 [] [ text budget.name ]
        , p [] [ text <| "Balance: " ++ toString budget.startingBalance ]
        , button [ onClick AddCategory ] [ text "New Category" ]
        ]


viewMonth : Month -> Html Msg
viewMonth month =
    div []
        [ button [ onClick PrevMonth ] [ text "<<" ]
        , span [] [ text <| Month.toString month ]
        , button [ onClick NextMonth ] [ text ">>" ]
        ]


view : Model -> Html Msg
view model =
    div []
        [ viewMonth model.month
        , viewBudget model.budget
        , div [] <| List.map (viewCategory model.month) model.budget.categories
        ]
