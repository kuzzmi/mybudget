module Amount exposing (Amount, add, code, generator, negate, toString, value, zero)

import Money exposing (Code, currencyFromCode)
import Random


type Amount
    = Amount Code Int


code : Amount -> Code
code val =
    case val of
        Amount code _ ->
            code


value : Amount -> Int
value val =
    case val of
        Amount _ v ->
            v


zero : Code -> Amount
zero code =
    Amount code 0


generator : Code -> Random.Generator Amount
generator code =
    Random.map (Amount code) <| Random.int 0 40000


negate : Amount -> Amount
negate a =
    case a of
        Amount c v ->
            Amount c <| Basics.negate v


add : Amount -> Amount -> Amount
add a1 a2 =
    let
        code_ =
            code a1

        sum =
            value a1 + value a2
    in
    Amount code_ sum


toString : Amount -> String
toString v =
    case v of
        Amount code value ->
            let
                currency =
                    currencyFromCode code

                divideBy =
                    10 ^ currency.decimalDigits

                decimals =
                    String.padLeft 2 '0' <| Basics.toString (value % divideBy)

                whole =
                    Basics.toString <| value // divideBy

                string =
                    if currency.decimalDigits == 0 then
                        whole
                    else
                        whole ++ "." ++ decimals
            in
            currency.symbol ++ string
