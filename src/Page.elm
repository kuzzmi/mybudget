module Page exposing (Msg, Page(..), State(..), fromRoute, update, view)

import Html.Styled as Html exposing (..)
import Page.Budget as Budget
import Page.Root as Root
import Router exposing (Route)


type Page
    = Root
    | NotFound
    | NewBudget Budget.Model


type State
    = Loaded Page


fromRoute : Router.Route -> Page
fromRoute route =
    case route of
        Router.NotFound ->
            NotFound

        Router.Root ->
            Root

        Router.NewBudget ->
            NewBudget Budget.init


update : Msg -> State -> ( State, Cmd Msg )
update msg state =
    case ( msg, state ) of
        ( UpdateRoot msg, _ ) ->
            let
                cmd =
                    Root.update msg
            in
            ( state, Cmd.map UpdateRoot cmd )

        ( UpdateBudget msg, Loaded (NewBudget model) ) ->
            let
                ( model_, cmd ) =
                    Budget.update msg model
            in
            ( Loaded (NewBudget model_), Cmd.map UpdateBudget cmd )

        _ ->
            ( state, Cmd.none )


type Msg
    = UpdateRoot Root.Msg
    | UpdateBudget Budget.Msg


view : State -> Html Msg
view state =
    case state of
        Loaded Root ->
            Html.map UpdateRoot Root.view

        Loaded NotFound ->
            h1 [] [ text "Not Found" ]

        Loaded (NewBudget model) ->
            Html.map UpdateBudget <| Budget.view model
