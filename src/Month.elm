module Month exposing (Month, empty, fromDate, next, prev, toPair, toString, urlParser)

import Date exposing (Month(..))
import UrlParser exposing (Parser)


type alias Month =
    ( Int, Int )


prev : Month -> Month
prev ( year, month ) =
    case month of
        1 ->
            ( year - 1, 12 )

        _ ->
            ( year, month - 1 )


next : Month -> Month
next ( year, month ) =
    case month of
        12 ->
            ( year + 1, 12 )

        _ ->
            ( year, month + 1 )


monthToInt : Date.Month -> Int
monthToInt m =
    case m of
        Jan ->
            1

        Feb ->
            2

        Mar ->
            3

        Apr ->
            4

        May ->
            5

        Jun ->
            6

        Jul ->
            7

        Aug ->
            8

        Sep ->
            9

        Oct ->
            10

        Nov ->
            11

        Dec ->
            12


intToMonth : Int -> Date.Month
intToMonth m =
    case m of
        1 ->
            Jan

        2 ->
            Feb

        3 ->
            Mar

        4 ->
            Apr

        5 ->
            May

        6 ->
            Jun

        7 ->
            Jul

        8 ->
            Aug

        9 ->
            Sep

        10 ->
            Oct

        11 ->
            Nov

        12 ->
            Dec

        _ ->
            Jan


intToString : Int -> String
intToString m =
    case m of
        1 ->
            "January"

        2 ->
            "February"

        3 ->
            "March"

        4 ->
            "April"

        5 ->
            "May"

        6 ->
            "June"

        7 ->
            "July"

        8 ->
            "August"

        9 ->
            "September"

        10 ->
            "October"

        11 ->
            "November"

        12 ->
            "December"

        _ ->
            "January"


empty : Month
empty =
    ( 0, 0 )


fromYearAndMonth : Int -> Date.Month -> Month
fromYearAndMonth year month =
    ( year, monthToInt month )


fromPair : ( Int, Int ) -> Month
fromPair ( year, month ) =
    fromYearAndMonth year (intToMonth month)


toPair : Month -> ( Int, Int )
toPair ( year, month ) =
    ( year, month )


fromDate : Date.Date -> Month
fromDate date =
    let
        year =
            Date.year date

        month =
            Date.month date
    in
    fromYearAndMonth year month


toString : Month -> String
toString ( year, month ) =
    Basics.toString year ++ " " ++ intToString month


urlParser : Parser (Month -> b) b
urlParser =
    UrlParser.custom "MONTH" <|
        \segment ->
            let
                pair =
                    String.split "-" segment
            in
            if List.length pair /= 2 then
                Err "Not a comma separated year and month"
            else
                case pair of
                    [ yearStr, monthStr ] ->
                        let
                            year =
                                String.toInt yearStr

                            month =
                                String.toInt monthStr
                        in
                        case ( year, month ) of
                            ( Ok y, Ok m ) ->
                                Ok <| fromPair ( y, m )

                            _ ->
                                Err "Something is wrong with data"

                    _ ->
                        Err "Something is wrong with data"
