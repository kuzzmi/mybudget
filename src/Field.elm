module Field exposing (Field, empty, get, map, pure, set, withDefault)


type Field a
    = Untouched
    | Invalid String a
    | Valid a


empty : Field a
empty =
    Untouched


set : a -> Field a -> (a -> Maybe String) -> Field a
set v field validation =
    let
        e =
            validation v
    in
    case e of
        Just error ->
            Invalid error v

        Nothing ->
            Valid v


get : Field a -> Maybe a
get field =
    case field of
        Valid a ->
            Just a

        Invalid _ a ->
            Just a

        Untouched ->
            Nothing


withDefault : a -> Field a -> a
withDefault default field =
    let
        v =
            get field
    in
    case v of
        Just a ->
            a

        Nothing ->
            default


pure : a -> Field a
pure v =
    Valid v


map : (a -> b) -> Field a -> Field b
map fn field =
    case field of
        Valid a ->
            Valid (fn a)

        Invalid er v ->
            Invalid er (fn v)

        Untouched ->
            Untouched
