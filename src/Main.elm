module Main exposing (..)

import Html.Styled as Html exposing (..)
import Navigation exposing (Location)
import Page
import Router exposing (Route(..))


main : Program Never Model Msg
main =
    Navigation.program
        (Router.parse >> SetRoute)
        { init = init
        , view = view >> toUnstyled
        , update = update
        , subscriptions = subscriptions
        }


subscriptions : Model -> Sub msg
subscriptions model =
    Sub.none



-- MODEL


type alias Model =
    { route : Route
    , pageState : Page.State
    }


model : Model
model =
    { route = Root
    , pageState = Page.Loaded Page.Root
    }


cmd : Cmd Msg
cmd =
    Cmd.none


init : Location -> ( Model, Cmd Msg )
init location =
    let
        route =
            Router.parse location
    in
    ( { model
        | route = route
        , pageState = Page.Loaded <| Page.fromRoute route
      }
    , cmd
    )



-- UPDATE


type Msg
    = SetRoute Route
    | UpdatePage Page.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetRoute route ->
            let
                page =
                    Page.fromRoute route
            in
            ( { model | route = route, pageState = Page.Loaded page }, Cmd.none )

        UpdatePage msg ->
            let
                ( state_, cmd ) =
                    Page.update msg model.pageState
            in
            ( { model | pageState = state_ }, Cmd.map UpdatePage cmd )



-- VIEW


view : Model -> Html Msg
view model =
    Html.map UpdatePage <| Page.view model.pageState
