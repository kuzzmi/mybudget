module Transaction exposing (Id, Transaction(..), generate, generator, value)

import Amount exposing (Amount)
import Money
import Random


type Id
    = Id Int


type Transaction
    = Expenditure Id Amount
    | Income Id Amount


value : Transaction -> Amount
value t =
    case t of
        Expenditure _ v ->
            v

        Income _ v ->
            v


generator : Random.Generator Transaction
generator =
    let
        idGenerator =
            Random.map Id <| Random.int -100 0
    in
    Random.map3
        (\x y z ->
            if x then
                Income y z
            else
                Expenditure y z
        )
        Random.bool
        idGenerator
        (Amount.generator Money.CHF)


generate : (Transaction -> msg) -> Cmd msg
generate msg =
    Random.generate msg generator
