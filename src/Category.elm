module Category
    exposing
        ( Category
        , Id
        , addNewMonthlyBudget
        , addTransaction
        , empty
        , getMonthlyBudget
        )

import Dict exposing (Dict)
import Month exposing (Month)
import MonthlyBudget exposing (MonthlyBudget)
import Transaction exposing (Transaction)


type Id
    = Id Int


type alias Category =
    { id : Id
    , name : String
    , monthlyBudgets : Dict Month MonthlyBudget
    }


getMonthlyBudget : Month -> Category -> Maybe MonthlyBudget
getMonthlyBudget month category =
    Dict.get month category.monthlyBudgets


empty : Category
empty =
    { id = Id -1
    , name = "Category"
    , monthlyBudgets = Dict.empty
    }


addNewMonthlyBudget : Month -> Category -> Category
addNewMonthlyBudget month category =
    let
        mbs =
            category.monthlyBudgets
    in
    if Dict.member month mbs then
        category
    else
        { category | monthlyBudgets = Dict.insert month MonthlyBudget.empty mbs }


addTransaction : Transaction -> Month -> Category -> Category
addTransaction t month category =
    let
        budget =
            Maybe.withDefault MonthlyBudget.empty <| getMonthlyBudget month category

        budgetWithTransaction =
            { budget | transactions = t :: budget.transactions }
    in
    { category
        | monthlyBudgets =
            Dict.insert
                month
                budgetWithTransaction
                category.monthlyBudgets
    }
