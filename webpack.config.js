const path = require('path');
const webpack = require('webpack');

module.exports = {
    mode: 'development',
    entry: './src/index.js',

    output: {
        path: path.resolve(__dirname, './dist/'),
        filename: 'bundle.js',
    },

    devServer: {
        contentBase: path.resolve(__dirname, './dist/'),
        compress: true,
        port: 8000,
        historyApiFallback: true,
    },

    module: {
        rules: [{
            test: /\.elm$/,
            exclude: [/elm-stuff/, /node_modules/],
            use: [{
                loader: 'elm-webpack-loader',
                options: {
                    verbose: true,
                    warn: true,
                    debug: true,
                },
            }],
        }, {
            test: /\.css$/,
            loader: 'style-loader!css-loader',
        }, {
            test: /\.scss$/,
            loader: 'style-loader!css-loader!sass-loader',
        }],
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
    ],
};
